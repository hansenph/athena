// This file's extension implies that it's C, but it's really -*- C++ -*-.

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SGTOOLS_STORABLECONVERSIONS_H
# define SGTOOLS_STORABLECONVERSIONS_H
/** @file StorableConversions.h
 *  @brief convert to and from a SG storable
 * $Id: StorableConversions.h,v 1.13 2008-05-22 22:54:12 calaf Exp $
 * @author ATLAS Collaboration
 **/


#include "AthenaKernel/StorableConversions.h"


#endif // SGTOOLS_STORABLECONVERSIONS_H
